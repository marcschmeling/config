" ----------------
" GENERAL SETTINGS
" ----------------

" display line numbers on the left
set number

" enable relative number - together with 'set number' displays absolute number
" for active line
" set relativenumber
set norelativenumber

" setting cursor pipe or block depending on mode
let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_SR = "\<Esc>]50;CursorShape=2\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"

" enable status line
set laststatus=2

" highlight the current line
set cursorline

" enable visual wrapping
set wrap

" disable actually wrapping with new lines
set textwidth=0
set wrapmargin=0

" file encoding for displayed and saved files
set encoding=utf-8
set fileencoding=utf-8

" disable compatibility with vi
set nocompatible

" enable syntax highlighting
syntax on

" highlight searches
set hlsearch

" use case insensitive search, except when using capital letters
set ignorecase
set smartcase

" allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" when opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

" stop some movements from going to first char of line
set nostartofline

" save changes? when quitting
set confirm

" use visual bell instead of beeping when doing something wrong
set visualbell

" no flash or beep for visual bell
set t_vb=

" enable mouse in all modes
set mouse=a

" time out on keycodes, no timeout on mappings
set notimeout ttimeout ttimeoutlen=200

" tab behaviour 
set shiftwidth=4
set softtabstop=4
set expandtab

" copy and paste to and from system tray
set clipboard=unnamed
set clipboard=unnamedplus

" -----------------
" PLUGIN MANAGEMENT
" -----------------

call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'tomtom/tcomment_vim'

call plug#end()

" --------
" MAPPINGS
" --------
" map leader
let mapleader="\\"

" make Y act yank to EOL, like D or C
map Y y$

" move current line down
no <down> ddp

" move current line up
no <up> ddkP

" move charakter left
no <left> xhhp

" move character right
no <right> xp

" new line underneath, but staying on current line
nmap <leader>o o<ESC>k

" new line above, but staying on current line
nmap <leader>O O<ESC>j

" center active line in the middle of the screen
nmap G Gzz
nmap n nzz
nmap N Nzz
nmap } }zz
nmap { {zz
nmap <C-d> <C-d>zz
nmap <C-u> <C-u>zz

" window splits 
nnoremap <Leader>v :vsplit<CR>
nnoremap <Leader>h :split<CR>

" move cursor to window
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-k> <C-w>k

" move to tabs
no <leader>1 1gt
no <leader>2 2gt
